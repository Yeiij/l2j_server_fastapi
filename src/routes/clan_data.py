from fastapi import APIRouter
from fastapi_pagination import (
    add_pagination,
    Page,
    paginate,
)

from models.ClanData import get_data
from schemas.IClanData import IClanData

clan_data = APIRouter()


@clan_data.get('/', response_model=Page[IClanData])
def clan_data_get_all(clan_name: str | None = None):
    return paginate(get_data(clan_name))


add_pagination(clan_data)
