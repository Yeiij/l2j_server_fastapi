from fastapi import APIRouter
from fastapi_pagination import (
    add_pagination,
    Page,
    paginate,
)

from models.Characters import get_data
from schemas.ICharacters import ICharacters

characters = APIRouter()


@characters.get('/', response_model=Page[ICharacters])
def characters_get_all(char_name: str | None = None):
    return paginate(get_data(char_name))


add_pagination(characters)
