from fastapi import APIRouter
from fastapi_pagination import (
    add_pagination,
    Page,
    paginate,
)

from models.GrandbossData import get_data
from schemas.IGrandBossData import IGrandBossData

grandboss_data = APIRouter()


@grandboss_data.get('/', response_model=Page[IGrandBossData])
def get_grandboss_data(boss_id: int | None = None):
    return paginate(get_data(boss_id))


add_pagination(grandboss_data)
