from fastapi import APIRouter
from fastapi_pagination import (
    add_pagination,
    Page,
    paginate,
)

from models.RaidbossSpawnlist import get_data
from schemas.IRaidbossSpawnlist import IRaidbossSpawnlist

raidboss_spawnlist = APIRouter()


@raidboss_spawnlist.get('/', response_model=Page[IRaidbossSpawnlist])
def raidboss_spawnlist_get_all(boss_id: int | None = None):
    return paginate(get_data(boss_id))


add_pagination(raidboss_spawnlist)
