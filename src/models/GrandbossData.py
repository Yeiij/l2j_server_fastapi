from sqlalchemy import (
    Column,
    DECIMAL,
    text,
)
from sqlalchemy.dialects.mysql import (
    BIGINT,
    MEDIUMINT,
    SMALLINT,
    TINYINT,
)
from sqlalchemy.orm import (
    declarative_base,
    Session,
)

from config.db import engine

Base = declarative_base()


class GrandbossData(Base):
    __tablename__ = 'grandboss_data'

    boss_id = Column(SMALLINT(5), primary_key=True)
    loc_x = Column(MEDIUMINT(6), nullable=False)
    loc_y = Column(MEDIUMINT(6), nullable=False)
    loc_z = Column(MEDIUMINT(6), nullable=False)
    heading = Column(MEDIUMINT(6), nullable=False, server_default=text("0"))
    respawn_time = Column(BIGINT(13), nullable=False, server_default=text("0"))
    currentHP = Column(DECIMAL(30, 15), nullable=False)
    currentMP = Column(DECIMAL(30, 15), nullable=False)
    status = Column(TINYINT(1), nullable=False, server_default=text("0"))


def get_data(boss_id: int = None):
    session = Session(engine)
    if boss_id:
        data = session.query(GrandbossData).where(
            GrandbossData.boss_id == boss_id).all()
    else:
        data = session.query(GrandbossData).all()
    session.close()
    return data
