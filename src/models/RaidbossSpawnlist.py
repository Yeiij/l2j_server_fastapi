from sqlalchemy import (
    Column,
    DECIMAL,
    text,
)
from sqlalchemy.dialects.mysql import (
    BIGINT,
    MEDIUMINT,
    SMALLINT,
    TINYINT,
)
from sqlalchemy.orm import (
    declarative_base,
    Session,
)

from config.db import engine

Base = declarative_base()


class RaidbossSpawnlist(Base):
    __tablename__ = 'raidboss_spawnlist'

    boss_id = Column(SMALLINT(5), primary_key=True)
    amount = Column(TINYINT(1), nullable=False, server_default=text("1"))
    loc_x = Column(MEDIUMINT(6), nullable=False)
    loc_y = Column(MEDIUMINT(6), nullable=False)
    loc_z = Column(MEDIUMINT(6), nullable=False)
    heading = Column(MEDIUMINT(6), nullable=False, server_default=text("0"))
    respawn_delay = Column(MEDIUMINT(6), nullable=False,
                           server_default=text("129600"))
    respawn_random = Column(MEDIUMINT(6), nullable=False,
                            server_default=text("86400"))
    respawn_time = Column(BIGINT(13), nullable=False, server_default=text("0"))
    currentHp = Column(DECIMAL(8, 0))
    currentMp = Column(DECIMAL(8, 0))


def get_data(boss_id: int = None):
    session = Session(engine)
    if boss_id:
        data = session.query(RaidbossSpawnlist).where(
            RaidbossSpawnlist.boss_id == boss_id).all()
    else:
        data = session.query(RaidbossSpawnlist).all()
    session.close()
    return data
