from sqlalchemy import (
    Column,
    String,
    text,
    TIMESTAMP,
)
from sqlalchemy.dialects.mysql import (
    BIGINT,
    INTEGER,
    MEDIUMINT,
    SMALLINT,
    TINYINT,
)
from sqlalchemy.orm import (
    declarative_base,
    Session,
)

from config.db import engine

Base = declarative_base()


class Characters(Base):
    __tablename__ = 'characters'

    account_name = Column(String(45), index=True)
    charId = Column(INTEGER(10), primary_key=True, server_default=text("0"))
    char_name = Column(String(35), nullable=False, index=True)
    level = Column(TINYINT(3))
    maxHp = Column(MEDIUMINT(8))
    curHp = Column(MEDIUMINT(8))
    maxCp = Column(MEDIUMINT(8))
    curCp = Column(MEDIUMINT(8))
    maxMp = Column(MEDIUMINT(8))
    curMp = Column(MEDIUMINT(8))
    face = Column(TINYINT(3))
    hairStyle = Column(TINYINT(3))
    hairColor = Column(TINYINT(3))
    sex = Column(TINYINT(3))
    heading = Column(MEDIUMINT(9))
    x = Column(MEDIUMINT(9))
    y = Column(MEDIUMINT(9))
    z = Column(MEDIUMINT(9))
    exp = Column(BIGINT(20), server_default=text("0"))
    expBeforeDeath = Column(BIGINT(20), server_default=text("0"))
    sp = Column(INTEGER(10), nullable=False, server_default=text("0"))
    karma = Column(INTEGER(10))
    fame = Column(MEDIUMINT(8), nullable=False, server_default=text("0"))
    pvpkills = Column(SMALLINT(5))
    pkkills = Column(SMALLINT(5))
    clanid = Column(INTEGER(10), index=True)
    race = Column(TINYINT(3))
    classid = Column(TINYINT(3))
    base_class = Column(TINYINT(3), nullable=False, server_default=text("0"))
    transform_id = Column(SMALLINT(5), nullable=False,
                          server_default=text("0"))
    deletetime = Column(BIGINT(13), nullable=False, server_default=text("0"))
    cancraft = Column(TINYINT(3))
    title = Column(String(21))
    title_color = Column(MEDIUMINT(8), nullable=False,
                         server_default=text("15530402"))
    accesslevel = Column(MEDIUMINT(9), server_default=text("0"))
    online = Column(TINYINT(3), index=True)
    onlinetime = Column(INTEGER(11))
    char_slot = Column(TINYINT(3))
    newbie = Column(MEDIUMINT(8), server_default=text("1"))
    lastAccess = Column(BIGINT(13), nullable=False, server_default=text("0"))
    clan_privs = Column(MEDIUMINT(8), server_default=text("0"))
    wantspeace = Column(TINYINT(3), server_default=text("0"))
    isin7sdungeon = Column(TINYINT(3), nullable=False,
                           server_default=text("0"))
    power_grade = Column(TINYINT(3))
    nobless = Column(TINYINT(3), nullable=False, server_default=text("0"))
    subpledge = Column(SMALLINT(6), nullable=False, server_default=text("0"))
    lvl_joined_academy = Column(TINYINT(3), nullable=False,
                                server_default=text("0"))
    apprentice = Column(INTEGER(10), nullable=False, server_default=text("0"))
    sponsor = Column(INTEGER(10), nullable=False, server_default=text("0"))
    clan_join_expiry_time = Column(BIGINT(13), nullable=False,
                                   server_default=text("0"))
    clan_create_expiry_time = Column(BIGINT(13), nullable=False,
                                     server_default=text("0"))
    death_penalty_level = Column(SMALLINT(5), nullable=False,
                                 server_default=text("0"))
    bookmarkslot = Column(SMALLINT(5), nullable=False,
                          server_default=text("0"))
    vitality_points = Column(SMALLINT(5), nullable=False,
                             server_default=text("0"))
    hunting_bonus = Column(INTEGER(10), nullable=False,
                           server_default=text("0"))
    nevit_blessing_points = Column(INTEGER(10), nullable=False,
                                   server_default=text("0"))
    nevit_blessing_time = Column(INTEGER(10), nullable=False,
                                 server_default=text("0"))
    createDate = Column(TIMESTAMP, nullable=False,
                        server_default=text("current_timestamp()"))
    language = Column(String(2))


def get_data(char_name: str = None):
    session = Session(engine)
    if char_name:
        data = session.query(Characters).where(
            Characters.char_name.like(char_name)).all()
    else:
        data = session.query(Characters).all()
    session.close()
    return data
