from sqlalchemy import (
    Column,
    String,
    text,
)
from sqlalchemy.dialects.mysql import (
    BIGINT,
    INTEGER,
    SMALLINT,
    TINYINT,
)
from sqlalchemy.orm import (
    declarative_base,
    Session,
)

from config.db import engine

Base = declarative_base()


class ClanData(Base):
    __tablename__ = 'clan_data'

    clan_id = Column(INTEGER(11), primary_key=True, server_default=text("0"))
    clan_name = Column(String(45))
    clan_level = Column(INTEGER(11))
    reputation_score = Column(INTEGER(11), nullable=False,
                              server_default=text("0"))
    hasCastle = Column(INTEGER(11))
    blood_alliance_count = Column(SMALLINT(5), nullable=False,
                                  server_default=text("0"))
    blood_oath_count = Column(SMALLINT(5), nullable=False,
                              server_default=text("0"))
    ally_id = Column(INTEGER(11), index=True)
    ally_name = Column(String(45))
    leader_id = Column(INTEGER(11), index=True)
    crest_id = Column(INTEGER(11))
    crest_large_id = Column(INTEGER(11))
    ally_crest_id = Column(INTEGER(11))
    auction_bid_at = Column(INTEGER(11), nullable=False, index=True,
                            server_default=text("0"))
    ally_penalty_expiry_time = Column(BIGINT(13), nullable=False,
                                      server_default=text("0"))
    ally_penalty_type = Column(TINYINT(1), nullable=False,
                               server_default=text("0"))
    char_penalty_expiry_time = Column(BIGINT(13), nullable=False,
                                      server_default=text("0"))
    dissolving_expiry_time = Column(BIGINT(13), nullable=False,
                                    server_default=text("0"))
    new_leader_id = Column(INTEGER(10), nullable=False,
                           server_default=text("0"))


def get_data(clan_name: str = None):
    session = Session(engine)
    if clan_name:
        data = session.query(ClanData).where(
            ClanData.clan_name.like(clan_name)).all()
    else:
        data = session.query(ClanData).all()
    session.close()
    return data
