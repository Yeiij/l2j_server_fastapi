from decimal import Decimal

from pydantic import BaseModel


class IRaidbossSpawnlist(BaseModel):
    boss_id: int
    amount: int
    loc_x: int
    loc_y: int
    loc_z: int
    heading: int
    respawn_delay: int
    respawn_random: int
    respawn_time: int
    currentHp: Decimal
    currentMp: Decimal

    class Config:
        orm_mode = True
