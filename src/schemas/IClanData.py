from pydantic import BaseModel


class IClanData(BaseModel):
    clan_id: int
    clan_name: str
    clan_level: int
    reputation_score: int
    hasCastle: int
    blood_alliance_count: int
    blood_oath_count: int
    ally_id: int
    ally_name: str
    leader_id: int
    crest_id: int
    crest_large_id: int
    ally_crest_id: int
    auction_bid_at: int
    ally_penalty_expiry_time: int
    ally_penalty_type: int
    char_penalty_expiry_time: int
    dissolving_expiry_time: int
    new_leader_id: int

    class Config:
        orm_mode = True
