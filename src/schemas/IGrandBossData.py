from pydantic import BaseModel


class IGrandBossData(BaseModel):
    boss_id: int
    loc_x: int
    loc_y: int
    loc_z: int
    heading: int
    respawn_time: int
    currentHP: int
    currentMP: int
    status: int

    class Config:
        orm_mode = True
