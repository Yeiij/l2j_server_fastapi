from typing import Optional

from pydantic import BaseModel


class ICharacters(BaseModel):
    account_name: str
    charId: int
    char_name: str
    level: int
    maxHp: int
    curHp: int
    maxCp: int
    curCp: int
    maxMp: int
    curMp: int
    face: int
    hairStyle: int
    hairColor: int
    sex: int
    heading: int
    x: int
    y: int
    z: int
    exp: int
    expBeforeDeath: int
    sp: int
    karma: int
    fame: int
    pvpkills: int
    pkkills: int
    clanid: int
    race: int
    classid: int
    base_class: int
    transform_id: int
    deletetime: int
    cancraft: int
    title: str
    title_color: int
    accesslevel: int
    online: int
    onlinetime: int
    char_slot: Optional[int]
    newbie: int
    lastAccess: int
    clan_privs: int
    wantspeace: int
    isin7sdungeon: int
    power_grade: int
    nobless: int
    subpledge: int
    lvl_joined_academy: int
    apprentice: int
    sponsor: int
    clan_join_expiry_time: int
    clan_create_expiry_time: int
    death_penalty_level: int
    bookmarkslot: int
    vitality_points: int
    hunting_bonus: int
    nevit_blessing_points: int
    nevit_blessing_time: int
    createDate = str
    language: Optional[str]

    class Config:
        orm_mode = True
