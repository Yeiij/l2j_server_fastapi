from fastapi import FastAPI

from routes.characters import characters
from routes.clan_data import clan_data
from routes.grandboss_data import grandboss_data
from routes.raidboss_spawnlist import raidboss_spawnlist

app = FastAPI()
app.include_router(characters, prefix='/api/characters')
app.include_router(clan_data, prefix='/api/clan_data')
app.include_router(grandboss_data, prefix='/api/grandboss_data')
app.include_router(raidboss_spawnlist, prefix='/api/raidboss_spawnlist')
