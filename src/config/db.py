import os

from dotenv import load_dotenv
from sqlalchemy import create_engine

load_dotenv()

DB_ENGI = os.getenv('DB_ENGI')
DB_HOST = os.getenv('DB_HOST')
DB_NAME = os.getenv('DB_NAME')
DB_PASS = os.getenv('DB_PASS')
DB_PORT = os.getenv('DB_PORT')
DB_USER = os.getenv('DB_USER')

DB_URL = f'{DB_ENGI}://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}'

engine = create_engine(DB_URL)
