# L2JServer API
`l2j_server_fastapi`  
API implementation for [L2JServer](https://www.l2jserver.com/)

+ [Python 3.10+](https://www.python.org/)
+ [FastAPI](https://fastapi.tiangolo.com/)
+ [MariaDB 10+](https://mariadb.org/)




## Configurations
### .env file
Create a `.env` file inside the `src` folder with the next content:
```dotenv
DB_ENGI=mariadb+pymysql
DB_HOST=localhost
DB_NAME=l2jgs
DB_PASS=l2jserver2019
DB_PORT=3306
DB_USER=l2j
```

## Build and Run
```shell
cd l2j_server_fastapi
pip install -r requirements.txt
cd src
uvicorn main:app
```
It is recommended to create a [Python virtual environment](https://docs.python.org/3.11/tutorial/venv.html)


## API URL's

ALL THESE URLS ACCEPTS **GET** REQUESTS ONLY

### Navegate de API
[Docs](http://127.0.0.1:8000/docs) `http://127.0.0.1:8000/docs`

### Player Related
[Characters with pagination](http://127.0.0.1:8000/api/characters/?page=1&size=10) `http://127.0.0.1:8000/api/characters/?page=1&size=10`  
[Characters by Name](http://127.0.0.1:8000/api/characters/?char_name=MaysoN&page=1&size=10) `http://127.0.0.1:8000/api/characters/?char_name=MaysoN&page=1&size=10`

### Clan Related
[Clans](http://127.0.0.1:8000/api/clan_data/?page=1&size=10) `http://127.0.0.1:8000/api/clan_data/?page=1&size=10`  
[Clans by Name](http://127.0.0.1:8000/api/clan_data/?clan_name=BaNNeD&page=1&size=10) `http://127.0.0.1:8000/api/clan_data/?clan_name=BaNNeD&page=1&size=10
`

### Grand Boss Related

[Grand Boses](http://127.0.0.1:8000/api/grandboss_data/?page=1&size=10) `http://127.0.0.1:8000/api/grandboss_data/?page=1&size=10`  
[Grand Boss by ID](http://127.0.0.1:8000/api/grandboss_data/?boss_id=29001&page=1&size=10) `http://127.0.0.1:8000/api/grandboss_data/?boss_id=29001&page=1&size=10`

### Raid Boss Related
[Raidboses](http://127.0.0.1:8000/api/raidboss_spawnlist/?page=1&size=10) `http://127.0.0.1:8000/api/raidboss_spawnlist/?page=1&size=10`  
[Raidboss by ID](http://127.0.0.1:8000/api/raidboss_spawnlist/?boss_id=25001&page=1&size=10) `http://127.0.0.1:8000/api/raidboss_spawnlist/?boss_id=25001&page=1&size=10`
